package pl.arapso.generators.server;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pl.arapso.generators.pesel.PeselGenerator;
import pl.arapso.generators.pesel.PeselValidator;

@RestController(value = "/generator")
public class GeneratorController {

	@RequestMapping("/pesel")
	public String generatePesel(@RequestParam(defaultValue = EMPTY) final String birthDate,
			@RequestParam(defaultValue = EMPTY) String gender) {
		LOG.info("> Generat pesel birthDate={}, gender={}", birthDate, gender);
		PeselGenerator generator = new PeselGenerator();
		String pesel = EMPTY;
		if (isNotBlank(birthDate) && isNotBlank(gender)) {
			pesel = generator.generate(birthDate, gender);
		} else if (isNotBlank(birthDate)) {
			pesel = generator.generateByBirthDate(birthDate);
		} else if (isNotBlank(gender)) {
			pesel = generator.generateByGender(gender);
		}
		if (!new PeselValidator().validate(pesel)) {
			LOG.error("Return pesel is not valid");
		}
		LOG.info("< Return pesel={}", pesel);
		return pesel;
	}

	@ExceptionHandler(value = IllegalArgumentException.class)
	public String handleException() {
		return "Invalid parameters";
	}

	private static final Logger LOG = LoggerFactory.getLogger(GeneratorController.class);

}
