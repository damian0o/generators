package pl.arapso.generators.pesel;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;

public class PeselValidator {

	public boolean validate(String pesel) {
		if (StringUtils.isEmpty(pesel)) {
			return false;
		}
		if (pesel.length() != 11) {
			return false;
		}

		if (!pesel.matches("\\d{11}")) {
			return false;
		}

		if (!isChecksumValid(pesel)) {
			return false;
		}

		if (isFromExclusionList(pesel)) {
			return false;
		}

		try {
			SimpleDateFormat peselDateFormat = new SimpleDateFormat("yyMMdd");
			peselDateFormat.parse(pesel.substring(0, 5));
		} catch (ParseException e) {
			return false;
		}
		return true;
	}

	private static final String INVALID_PESEL_ZEROS_ONLY = "[0]{11}";

	/** Checksum coefficients. */
	private static final int[] COEFS = new int[] {
			1, 3, 7, 9, 1, 3, 7, 9, 1, 3
	};

	private boolean isFromExclusionList(final String pesel) {
		if (StringUtils.isNotBlank(pesel) && pesel.matches(INVALID_PESEL_ZEROS_ONLY)) {
			return true;
		}
		return false;
	}

	/**
	 * Calculates PESEL checksum and compares it to given value.
	 * @param pesel
	 * @return
	 */
	private boolean isChecksumValid(final String pesel) {
		final int psize = pesel.length();
		if (psize != 11) {
			return false;
		}
		int j = 0, sum = 0, control = 0;
		final int csum = Integer.valueOf(pesel.substring(psize - 1));
		for (int i = 0; i < psize - 1; i++) {
			final char c = pesel.charAt(i);
			j = Integer.valueOf(String.valueOf(c));
			sum += j * COEFS[i];
		}
		control = 10 - (sum % 10);
		if (control == 10) {
			control = 0;
		}
		return (control == csum);
	}
}
