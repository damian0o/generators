package pl.arapso.generators.pesel;

import static java.util.Arrays.asList;
import static java.util.Calendar.DAY_OF_YEAR;
import static java.util.Calendar.YEAR;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.GregorianCalendar;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

/**
 * @author dospara
 */
public class PeselGenerator {

	public static final String FEMALE_SYMBOL = "F";

	public static final String MALE_SYMBOL = "M";

	public String generate(final String birthDateString, final String gender) {
		final DateTime birthDate = parseDate(birthDateString);
		if (isInPeselValidInterval(birthDate) && isValidGender(gender)) {
			return generatePesel(birthDate, gender);
		}
		return EMPTY;
	}

	public String generateByBirthDate(final String birthDateString) {
		final DateTime birthDate = parseDate(birthDateString);
		if (isInPeselValidInterval(birthDate)) {
			return generatePesel(birthDate, getRandElement(new String[] { MALE_SYMBOL, FEMALE_SYMBOL }));
		}
		return EMPTY;
	}

	public String generateByGender(String gender) {
		if (isValidGender(gender)) {
			return generatePesel(getRandomDate(), gender);
		}
		return EMPTY;
	}

	static final Character[] MALE = { '1', '3', '5', '7', '9' };

	static final Character[] FEMALE = { '0', '2', '4', '6', '8' };

	private static <T> T getRandElement(T[] array) {
		return array[randBetween(0, array.length - 1)];
	}

	private static int randBetween(int start, int end) {
		return start + (int) Math.round(Math.random() * (end - start));
	}

	private static DateTime getRandomDate() {
		GregorianCalendar gc = new GregorianCalendar();
		int year = randBetween(MIN_YEAR, MAX_YEAR);
		gc.set(YEAR, year);
		int dayOfYear = randBetween(1, gc.getActualMaximum(DAY_OF_YEAR));
		gc.set(DAY_OF_YEAR, dayOfYear);
		return new DateTime(gc.getTime());
	}

	private static DateTime parseDate(String date) {
		if (isNotBlank(date)) {
			DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MM-yyyy");
			DateTime dt = formatter.parseDateTime(date);
			return dt;
		}
		throw new IllegalArgumentException("Date format not recognized");
	}

	private static final char ZERO = '0';

	private static final int MIN_YEAR = 1800;

	private static final int MAX_YEAR = 2299;

	private static final int[] WEIGHTS = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };

	private static final Map<Integer, Integer> CENTURY_MAP = ImmutableMap.of(19, 8, 20, 0, 21, 2, 22, 4, 23, 6);

	private String generatePesel(DateTime birthDate, String gender) {
		String string = getBirthDateDigits(birthDate) + getOrdinalDigits() + getGenderDigit(gender);
		return string + getCheckSumDigit(string);
	}

	private boolean isValidGender(String gender) {
		return asList(MALE_SYMBOL, FEMALE_SYMBOL).contains(gender);
	}

	private boolean isInPeselValidInterval(DateTime date) {
		DateTime start = new DateTime(MIN_YEAR, 1, 1, 0, 0, 0, 0);
		DateTime end = new DateTime(MAX_YEAR, 12, 31, 23, 59, 59, 59);
		Interval interval = new Interval(start, end);
		return interval.contains(date);
	}

	private String getBirthDateDigits(DateTime date) {
		DateTimeFormatter peselDateFormat = DateTimeFormat.forPattern("yyMMdd");
		StringBuilder builder = new StringBuilder(date.toString(peselDateFormat));

		int centuryOfEra = date.getCenturyOfEra() + 1;
		char mothMajor = builder.charAt(2);
		builder.setCharAt(2, String.valueOf(Character.getNumericValue(mothMajor) + CENTURY_MAP.get(centuryOfEra))
				.charAt(0));
		return builder.toString();
	}

	private String getGenderDigit(String gender) {
		if (MALE_SYMBOL.equalsIgnoreCase(gender)) {
			return String.valueOf(getRandElement(MALE));
		} else if (FEMALE_SYMBOL.equalsIgnoreCase(gender)) {
			return String.valueOf(getRandElement(FEMALE));
		}
		throw new IllegalArgumentException("Invalid gender symbol");
	}

	private String getOrdinalDigits() {
		String ordinal = String.valueOf(randBetween(0, 999));
		return Strings.padStart(ordinal, 3, ZERO);
	}

	private String getCheckSumDigit(String string) {
		int checkSum = 0;
		for (int i = 0; i < string.length(); i++) {
			checkSum += Character.getNumericValue(string.charAt(i)) * WEIGHTS[i];
		}
		checkSum %= 10;
		return String.valueOf((10 - checkSum) % 10);
	}

}
