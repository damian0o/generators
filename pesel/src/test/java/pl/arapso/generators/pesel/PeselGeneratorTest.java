package pl.arapso.generators.pesel;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.arapso.generators.pesel.PeselGenerator.FEMALE;
import static pl.arapso.generators.pesel.PeselGenerator.MALE;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.googlecode.zohhak.api.TestWith;
import com.googlecode.zohhak.api.runners.ZohhakRunner;

/**
 * @author dospara
 */
@RunWith(ZohhakRunner.class)
public class PeselGeneratorTest {

	@TestWith(value = { "23-11-1989, 891123", "01-01-1988, 880101",
			"01-01-2015, 152101", "01-12-2015, 153201", "01-12-1815, 159201",
			"01-01-1815, 158101", "01-10-1815, 159001" })
	public void shouldReturnPeselForBirthDate(String date, String expected) {
		String pesel = uut.generateByBirthDate(date);
		assertThat(pesel).startsWith(expected).hasSize(11);
		assertThat(peselValidator.validate(pesel)).isTrue();
	}
	
	@TestWith(value = { "01-01-1799", "01-01-2300" })
	public void shouldReturnEmptyForDateOutsideInterval(String date) {
		String pesel = uut.generateByBirthDate(date);
		assertThat(pesel).isEmpty();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldTrhowIllegalArgumentException(){
		uut.generateByBirthDate("");
	}
	
	@Test
	public void shouldGenerateGenderForMan() {
		String pesel = uut.generateByGender("M");
		assertThat(pesel.charAt(9)).isIn(MALE);
		assertThat(peselValidator.validate(pesel)).isTrue();
	}

	@Test
	public void shouldGenerateGenderForFemale() {
		String pesel = uut.generateByGender("F");
		assertThat(pesel.charAt(9)).isIn(FEMALE);
		assertThat(peselValidator.validate(pesel)).isTrue();
	}

	private PeselGenerator uut = new PeselGenerator();

	private PeselValidator peselValidator = new PeselValidator();

}
